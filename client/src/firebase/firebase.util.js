import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config= {
    apiKey: "AIzaSyDX9WRoHzUey7X9-BqKrofwqpmo4HhRE20",
    authDomain: "crwn-db-c18c2.firebaseapp.com",
    databaseURL: "https://crwn-db-c18c2.firebaseio.com",
    projectId: "crwn-db-c18c2",
    storageBucket: "crwn-db-c18c2.appspot.com",
    messagingSenderId: "565298098318",
    appId: "1:565298098318:web:f54a8cb9d686e51eafd9df",
    measurementId: "G-L63THBDKY0"
  };

  export const createUserProfileDocument = async (userAuth,additionalData) => {
      if (!userAuth) return;

      const userRef = firestore.doc(`users/${userAuth.uid}`);
      
      const snapShot = await userRef.get();

      if(!snapShot.exists){
        const {displayName,email} =  userAuth;
        const createAt = new Date();

        try {
            await userRef.set({
                displayName,
                email,
                createAt,
                ...additionalData
            })
        } catch (error) {
            console.log('Error creating user', error.message);
        }
      }
    
      return userRef;
  }

  export const addCollectionAndDocuments = async (collectionKey,objectToAdd) => {
    const collectionRef = firestore.collection(collectionKey); 
    const batch = firestore.batch();
    objectToAdd.forEach(element => {
        const newDocRef = collectionRef.doc();
        batch.set(newDocRef,element);
    });
    return await batch.commit(); 
  };

  export const convertCollectionSnapshotToMap =  (collection) =>{
    const transformedCollection = collection.docs.map(doc => {
        const { title, items } =  doc.data();
        return{
            routeName: encodeURI(title.toLowerCase()),
            id: doc.id,
            title,
            items
        };
    });
    return transformedCollection.reduce((accumulator,collection) => {
        accumulator[collection.title.toLowerCase()] = collection;
        return accumulator;
    },{});
  };

  export const getCurrentUser = () =>{
    return new Promise((resolve, reject) =>{
        const unsubscribe = auth.onAuthStateChanged(userAuth=>{
            unsubscribe();
            resolve(userAuth);
        },resolve);
    });
  }

  firebase.initializeApp(config);

  export const auth = firebase.auth();
  export const firestore  = firebase.firestore();

  export const googleProvider = new firebase.auth.GoogleAuthProvider();
  googleProvider.setCustomParameters({prompt:'select_account'});
  export const signInWithGoogle = () => auth.signInWithPopup(googleProvider);

  export default firebase;