import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import CartIcon from '../cart-icon/cart-icon';
import CartDropdown from '../cart-dropdown/cart-dropdown';
import { selectCurrentUser } from '../../redux/user/user.select';
import { selectCartHidden } from '../../redux/cart/cart.selector';
import { signOutStart } from '../../redux/user/user.action';

import { ReactComponent as Logo } from '../../assets/crown.svg';
import { HeaderConteiner,LogoConteiner,OptionConteiner,OptionLink }  from './header.style'


const Header = ({currentUser,hidden,signOutStart}) => (
    <HeaderConteiner>
        <LogoConteiner to="/">
            <Logo className='logo'/>
        </LogoConteiner>
        <OptionConteiner>
            <OptionLink to='/shop'>
                SHOP
            </OptionLink>
            <OptionLink to='/contact'>
                CONTACT
            </OptionLink>
            {
                currentUser ? (
                    <div className='option' onClick={signOutStart}>
                        SIGN OUT
                    </div>
                ):(
                    <OptionLink className='option' to='/signin'>
                        SIGN IN
                    </OptionLink>
                )
            }
            <CartIcon/>
        </OptionConteiner>
        {
            hidden ? null:  <CartDropdown/>
        }
        
    </HeaderConteiner>
)

const mapStateToProps = createStructuredSelector({
    currentUser : selectCurrentUser,
    hidden: selectCartHidden,
});

const mapDispatchToProps = dispatch => ({
    signOutStart: () => dispatch(signOutStart()),
});

export default connect(mapStateToProps,mapDispatchToProps)(Header);