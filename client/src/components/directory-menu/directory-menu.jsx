import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect'

import { selectDirectorySections } from '../../redux/directory/directory.selector'

import MenuItem from '../menu-item/menu-item';
import './directory-menu.scss';


const DirectoryMenu = ({sections}) =>(

  <div className='directory-menu'>
      {sections.map(({id, ...setionProps}) =>(
      <MenuItem key={id} {...setionProps}/>))
      }   
  </div>

);

const mapStateToProps =createStructuredSelector({
  sections: selectDirectorySections,
});



export default connect(mapStateToProps)(DirectoryMenu);
