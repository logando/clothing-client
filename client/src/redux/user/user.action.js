import UserActioType from './user.types'

export const googleSignInStart = () => ({
    type: UserActioType.GOOGLE_SIGN_IN_START
});

export const emailSignInStart = EmailAndPassword => ({
    type: UserActioType.EMAIL_SIGN_IN_START,
    payload: EmailAndPassword
});

export const signInSuccess = user => ({
    type: UserActioType.SIGN_IN_SUCCESS,
    payload: user
});

export const signInFailure = error => ({
    type: UserActioType.SIGN_IN_FAILURE,
    payload: error
});

export const checkUserSession = () => ({
    type: UserActioType.CHECK_USER_SESSION
})

export const signOutStart = () => ({
    type: UserActioType.SIGN_OUT_START,
});

export const signOutSuccess = () => ({
    type: UserActioType.SIGN_OUT_SUCCESS,
});

export const signOutFailure = error => ({
    type: UserActioType.SIGN_OUT_FAILURE,
    payload: error
});

export const signUpStart =(userCredentials) =>({
    type: UserActioType.SIGN_UP_START,
    payload:userCredentials
});


export const signUpSuccess = ({user,additionalData}) =>({
    type:UserActioType.SIGN_UP_SUCCESS,
    payload: { user, additionalData }
});

export const signUpFailure = error => ({
    type: UserActioType.signUpFailure,
    payload: error
});