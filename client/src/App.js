import React, { useEffect, lazy, Suspense  } from 'react';
import { Switch, Route,Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import Header from './components/header/header';
import Spinner from './components/spinner/spinner';
import ErrorBoundary from './components/error-boundary/error-boundary';

import { checkUserSession } from './redux/user/user.action'
import { selectCurrentUser } from './redux/user/user.select';

import { GlobalStyle } from './global.styles'

const SignInAndSignOutPage = lazy(()=>import('./pages/sign-in-and-sign-up/sign-in-and-sign-up'));
const checkoutPage = lazy(() => import('./pages/checkout/checkout'));
const HomePage = lazy(() => import('./pages/homepage/homepage'));
const ShopPage = lazy(() => import('./pages/shop/shopPage'));


const App = ({ checkUserSession, currentUser }) => {

  useEffect(()=>{
    checkUserSession();
  },[checkUserSession]);

  return (
    <div >
      <GlobalStyle/>
      <Header/>
      <Switch>
        <ErrorBoundary>
          <Suspense fallback={<Spinner/>}>
            <Route exact path='/' component={HomePage} />
            <Route path='/shop' component={ShopPage} />
            <Route exact path='/checkout' component={checkoutPage} />
            <Route exact path='/signin' render={()=> currentUser ? (<Redirect to='/' /> ) : (<SignInAndSignOutPage/>) } />
          </Suspense>
        </ErrorBoundary>
      </Switch>
    </div>
  );
}



const mapStatetoProps = createStructuredSelector({
  currentUser: selectCurrentUser, 
});

const mapDispatchToProps = dispatch => ({
  checkUserSession:  () => dispatch(checkUserSession())
})


export default connect(mapStatetoProps,mapDispatchToProps)(App);
