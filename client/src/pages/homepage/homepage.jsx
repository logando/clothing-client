import React from 'react';
import DirectoryMenu from '../../components/directory-menu/directory-menu';

import { HomePageConteiner } from './homepage.style'

const homepage = () =>{
    
    return (
        <HomePageConteiner >
            <DirectoryMenu/>
        </HomePageConteiner>
    );
}

export default homepage;